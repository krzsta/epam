import Core.WebDriverFactory;
import Pages.GooglePage;
import Pages.MantisPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

public class BaseTest {
    private WebDriver driver;
    private WebDriverFactory webDriverFactory;
    protected GooglePage googlePage;
    protected MantisPage mantisPage;

    @BeforeMethod
    @Parameters(("browser"))
    public void setUp(@Optional("firefox") String browser) {
        webDriverFactory = new WebDriverFactory();
        driver = webDriverFactory.getBaseWebDriver(browser);
        driver.manage().deleteAllCookies();

        googlePage = new GooglePage(driver);
        mantisPage = new MantisPage(driver);
    }

    @AfterMethod
    public void close() {
        driver.quit();
    }
}
