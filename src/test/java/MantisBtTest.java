import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MantisBtTest extends BaseTest {

    @DataProvider(name = "mantisProperValues")
    public static Object[][] mantisProperValues() {

        return new Object[][]{
                {"mantis", "sourceforge.net/projects/mantisbt/", "2.6"}
        };
    }

    @Test(dataProvider = "mantisProperValues")
    public void checkExpectedVersionHasTheMostNumbersOfDownload(String searchText, String searchLink, String expectedValue) {
        googlePage
                .goToGooglePage()
                .fillSearchFieldAndPressEnter(searchText)
                .navigateToMantisPageFromSearchResult(searchLink)
                .acceptPrivacy()
                .goToFiles()
                .goToStableVersion()
                .assertVersion(expectedValue);
    }
}
