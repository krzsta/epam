package Core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverFactory {

    public WebDriver getBaseWebDriver(String browser) {
        WebDriver driver;

        browser = browser.toLowerCase();
        String operatingSystem = System.getProperty("os.name").toLowerCase();

        if ("chrome".equals(browser)) {
            if ("linux".equals(operatingSystem)) {
                System.setProperty("webdriver.chrome.driver", Properties.linuxChromeDriverPath);
            } else if (operatingSystem.contains("windows")) {
                System.setProperty("webdriver.chrome.driver", Properties.windowsChromeDriverPath);
            } else {
                System.out.println("There is no WebDriver for " + operatingSystem);
            }
            driver = new ChromeDriver();

        } else {
            if ("linux".equals(operatingSystem)) {
                System.setProperty("webdriver.gecko.driver", Properties.linuxFirefoxDriverPath);
            } else if (operatingSystem.contains("windows")) {
                System.setProperty("webdriver.gecko.driver", Properties.windowsFirefoxDriverPath);
            } else {
                System.out.println("There is no WebDriver for " + operatingSystem);
            }
            driver = new FirefoxDriver();
        }

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }
}
