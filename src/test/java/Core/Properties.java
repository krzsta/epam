package Core;

public class Properties {
    // Drivers
    static String linuxFirefoxDriverPath = "./src/main/resources/drivers/geckodriver";
    static String windowsFirefoxDriverPath = "./src/main/resources/drivers/geckodriver.exe";
    static String linuxChromeDriverPath = "./src/main/resources/drivers/chromedriver";
    static String windowsChromeDriverPath = "./src/main/resources/drivers/chromedriver.exe";
}
