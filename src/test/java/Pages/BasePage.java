package Pages;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class BasePage {
    protected WebDriver driver;
    private int timeOutInSeconds = 20;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    protected void waitForElementToBeClickable(WebElement element) {
        new WebDriverWait(driver, timeOutInSeconds).until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitForElementVisibility(WebElement element) {
        new WebDriverWait(driver, timeOutInSeconds).until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitForElementInvisibility(WebElement element) {
        new WebDriverWait(driver, timeOutInSeconds).until(ExpectedConditions.invisibilityOf(element));
    }

    protected List getTextFromWebElementsList(List<WebElement> webElements, By by) {
        List list = new ArrayList<>();
        webElements.forEach(el -> list.add(el.findElement(by).getText()));
        return list;
    }

    protected List removeComaFromString(List list) {
        List newList = new ArrayList<>();
        list.forEach(el -> newList.add(el.toString().replace(",", "")));
        return newList;
    }

    protected List parseStringToInteger(List list) {
        List newList = new ArrayList<>();
        list.forEach(el -> newList.add(Integer.parseInt(StringUtils.substringBefore(el.toString(), " "))));
        return newList;
    }
}

