package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.Collections;
import java.util.List;

public class MantisPage extends BasePage {

    public MantisPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "qcCmpUi")
    private WebElement privacyPopUp;

    @FindBy(css = "#top_nav_admin > ul > li:nth-child(2) > a")
    private WebElement files;

    @FindBy(css = "#files_list > tbody > tr:nth-child(1) > th > a")
    private WebElement mantisStableFolder;

    @FindBy(xpath = "//*[@id=\"qcCmpButtons\"]/button[2]")
    private WebElement acceptPrivacyPolicyButton;

    @FindBy(css = "#files_list > tbody")
    private WebElement versionsContainer;

    private By row = By.cssSelector(".folder + tr");
    private By version = By.cssSelector("th > a > span");
    private By amountOfDownloads = By.cssSelector("div.stats.show-for-medium > a");


    public MantisPage acceptPrivacy() {
        waitForElementVisibility(privacyPopUp);
        waitForElementToBeClickable(acceptPrivacyPolicyButton);
        acceptPrivacyPolicyButton.click();
        waitForElementInvisibility(privacyPopUp);
        return this;
    }

    public MantisPage goToFiles() {
        waitForElementVisibility(files);
        waitForElementToBeClickable(files);
        files.click();
        return this;
    }

    public MantisPage goToStableVersion() {
        waitForElementToBeClickable(mantisStableFolder);
        mantisStableFolder.click();
        return this;
    }

    private String getVersionThatHasTheMostNumbersOfDownload() {
        List<WebElement> element = versionsContainer.findElements(row);
        List textFromWebElements = getTextFromWebElementsList(element, amountOfDownloads);
        List textFromWebElementsWithoutComa = removeComaFromString(textFromWebElements);
        List integers = parseStringToInteger(textFromWebElementsWithoutComa);

        int max = (int) Collections.max(integers);
        String version = element.get(integers.indexOf(max)).findElement(this.version).getText();

        System.out.println("Version number that has the most numbers of downloading is: " + version);

        return version;

    }

    public void assertVersion(String version) {
        String actualVersion = getVersionThatHasTheMostNumbersOfDownload();
        Assert.assertEquals(actualVersion, version);
    }
}
