package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GooglePage extends BasePage {

    public GooglePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "lst-ib")
    private WebElement searchField;

    @FindBy(id = "pnnext")
    private WebElement nextPageButton;

    @FindBy(id = "res")
    private WebElement searchResultContainer;

    private By searchResultLink = By.cssSelector("div > h3 > a");

    public GooglePage goToGooglePage() {
        driver.get("https://www.google.com/");
        return this;
    }

    public GooglePage fillSearchFieldAndPressEnter(String searchText) {
        searchField.sendKeys(searchText);
        searchField.sendKeys(Keys.ENTER);
        return this;
    }

    public MantisPage navigateToMantisPageFromSearchResult(String searchText) {
        WebElement webElement = checkLinksResult(searchText);
        webElement.click();
        return new MantisPage(driver);
    }

    private WebElement checkLinksResult(String searchUrl) {
        boolean contains;
        List<WebElement> webElements = getSearchResultsFromPage();

        for (WebElement webElement : webElements) {
            contains = webElement.getAttribute("href").contains(searchUrl);
            if (contains) {
                return webElement;
            }
        }

        nextPageButton.click();
        return checkLinksResult(searchUrl);
    }

    private List<WebElement> getSearchResultsFromPage() {
        waitForElementVisibility(searchResultContainer);
        return searchResultContainer.findElements(searchResultLink);
    }
}
